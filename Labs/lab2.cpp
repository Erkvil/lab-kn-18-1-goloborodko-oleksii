#include "pch.h"
#include <iostream>
#include <math.h>
using namespace std;
int main()
{
	float pi = 3.14;
	int x;
	cout << "Set x: "; cin >> x;
	float _sin = (x - 3) * pi / 180;
	int val = 3 + 2 * pow(_sin, 2) - 4 + x / 10;
	cout << "3+2sin^2(x-3)-4+x/10 = " << val << endl;
	return 0;
}
