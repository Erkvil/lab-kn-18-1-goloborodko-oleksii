#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <iomanip>
using namespace std;
int main()
{
	int z;
	float x, y;
	cout << "Set the z value: ";
	cin >> z;
	float F1 = pow(z, 3) - 3 * pow(z, 2);
	float F2 = sin(z*M_PI / 180);
	float F3 = pow(M_E, 2) - pow(M_E, -z);

	if (z < 0) {

		x = F1;
	}
	else if (0 <= z && z <= 8) {
		x = F2;
	}
	else if (z>8) {
		x = F3;
	}
	cout << "Result: " << x << endl;
	float MonthNumber;
	cout << "Enter the month number to find out the quarter: ";
	cin >> MonthNumber;
	int Quarter = MonthNumber / 3 + 1;
	//12,1,2 - Winter
	//3,4,5 - Spring
	//6,7,8 - Summer
	//9,10,11 - Autumn
	if (Quarter == 5) {
		Quarter = 1;
	}
	switch (Quarter)
	{
	case 1:
		cout << "Winter" << endl;
		break;
	case 2:
		cout << "Spring" << endl;
		break;
	case 3:
		cout << "Summer" << endl;
		break;
	case 4:
		cout << "Autumn" << endl;
		break;
	}
	cout << "\n";
	
     return 0;
}
