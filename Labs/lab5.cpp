#include <iostream>
using namespace std;
const int width = 4,
height = 4;
const int arr[height][width] = { { 342,33,43,5 },{ 21, 35, 61, 32 },{ 35,43,25,22 },{ 4,53,23,31 } };
int y[height] = { 0 };

int main(int argc, char *argv[]) {
	cout << "x:\n";
	int min;
	for (int i = 0; i < width; i++) {
		min = arr[0][i];
		for (int j = 0; j < height; j++) {
			cout << arr[i][j] << "\t";
			if (arr[j][i] < min) {
				min = arr[j][i];
				y[i] = min;
		}
	}
		cout << endl;
		}
	cout << "y:\n";
	for (int i = 0; i < width; i++) {
		cout << y[i] << "\t";
	}
	return 0;
}

