
#include <iostream>
#include <cmath>
using namespace std;
float powerm(float a, float b);
float e = 2.71;
float q(float &x) {
	return pow(x, 2) - powerm(e, -x);
}
float w(float *x) {
	return log(*x) + sqrt(*x);
}
float r(float *x) {
	return pow(cos(*x), 2) + pow(*x, 5);
}
void H(float val) {
	float a = q(val);
	float b = w(&val);
	float c = r(&val);
	cout << pow(a, 2) + pow(b, 2) - 6 * c;

}
float powerm(float a, float b) {
	float val = a;
	if (b > 0) {
		for (float i = 1; i < b; i++) {
			val = val * a;
		}
		return val;
	}
	else if (b < 0) {
		b = 0 - b;
		for (float i = 1; i < b; i++) {
			val = val * a;
		}
		val = 0 - val;
		return val;
	}
}
int main()
{
	int value;
	cout << "Set: ";
	cin >> value;
	H(value);

}
