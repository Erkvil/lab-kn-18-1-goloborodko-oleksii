#include "pch.h"
#include <iostream>
#include <string>
#include <cmath> 
#include  <fstream>
#include "conio.h"
 using namespace std;

void SecToTime(int n)
{
	int h, m, s;
	string hour, min, sec;
	h = n / 3600;
	m = n / 60;
	s = n % 3600;
	if (h > 24) h = h % 24;
	if (m > 60) m = m % 60;
	if (s > 60) s = s % 60;
	if (m < 10) min = "0" + to_string(m);
	else min = to_string(m);
	if (s < 10) sec = "0" + to_string(s);
	else sec = to_string(s);
	if (h < 10) hour = "0" + to_string(h);
	else hour = to_string(h);
	if (h == 24) hour = "00";
	if (m == 60) min = "00";
	if (s == 60) sec = "00";
	cout << hour << " hours " << min << " minutes " << sec << " seconds" << endl;
};


int main()
{

	string path = "Projekt.txt";
	
	ofstream fout;
	fout.open(path);
	fstream fs;
	fs.open(path, fstream::in | fstream::out | fstream::app);

	if (!fs.is_open())
	{
		cout << "Error" << endl;
	}
	else
	{
		cout << "Good" << endl;
	}


	float time, fractional, integer;         // Минуты в секунды
	cout << " Enter the minutes to clear the seconds: ";
	cin >> time;
	fractional = modf(time, &integer);
	cout << integer << " minutes " << " this " << integer * 60 + fractional * 100 << " seconds " << "\n";
	
	int timeSec;                             //Секунды в минуты
	cout << "Enter a time in seconds: ";
	cin >> timeSec;
	cout << "Target info:" << "\n";
	SecToTime(timeSec);
	system("pause");
	
	setlocale(LC_ALL, "");
	cout << "\t\t\tConverting a number from meters to kilometers\n\n\n";       //Первод метры в км и см
	float a;
	int x;
	cout << "Enter the number of meters :";
	cin >> a;
	cout << "\n";
	x = a;
	x = x % 10;
	if (x == 1)
		cout << a << " meter will be " << a / 1000 << " kilometers " << endl;
	if (x > 1 && x <= 4)
		cout << a << " meter will be " << a / 1000 << " kilometers " << endl;
	if (x >= 5 && x < 10)
		cout << a << " meter will be " << a / 1000 << " kilometers " << endl;
	if (a == 0)
		cout << a << " meter will be " << a / 1000 << " kilometers " << endl;
	cout << endl;

	cout << "\t\t\tFrom meters to centimeters\n\n\n";  // Метр в сантиметр 
	float b;
	int y;
	cout << "Enter the number of meters :";
	cin >> b;
	cout << "\n";
	y = b;
	y = y % 10;
	if (y == 1)
		cout << b << " meter will be " << b * 100 << " centimeters " << endl;
	if (y > 1 && y <= 4)
		cout << b << " meter will be " << b * 100 << " centimeters " << endl;
	if (y >= 5 && y < 10)
		cout << b << " meter will be " << b * 100 << " centimeters " << endl;
	if (b == 0)
		cout << b << " meter will be " << b * 100 << " centimeters " << endl;
	cout << endl;
	
	cout << "\t\t\tFrom kilometers to meter\n\n\n";    //Километр в метр 
	float c;
	int z;
	cout << "Enter the number of  kilometers: ";
	cin >> c;
	cout << "\n";
	z = c;
	z = z % 10;
	if (y == 1)
		cout << c << " kilometers will be " << c * 1000 << " meter " << endl;
	if (z > 1 && z <= 4)
		cout << c << " kilometers will be " << c * 1000 << " meter " << endl;
	if (z >= 5 && z < 10)
		cout << c << " kilometers will be " << c * 1000 << " meter " << endl;
	if (c == 0)
		cout << c << " kilometers will be " << c * 1000 << " meter " << endl;
	cout << endl;
	system("pause");
	
	cout << "\t\t\tFrom centimeters to millimeter\n\n\n";    //Сантиметр в милиметр  
	float d;
	int mi;
	cout << "Enter the number of  centimeters: ";
	cin >> d;
	cout << "\n";
	mi = d;
	mi = mi % 10;
	if (y == 1)
		cout << d << " centimeters will be " << d * 10 << " millimeter " << endl;
	if (mi > 1 && mi <= 4)
		cout << d << " centimeters will be " << d * 10 << " millimeter " << endl;
	if (mi >= 5 && mi < 10)
		cout << d << " centimeters will be " << d * 10 << " millimeter " << endl;
	if (d == 0)
		cout << d << " centimeters will be " << d * 10 << " millimeter " << endl;
	cout << endl;
	system("pause");
	
	int mas;                   // Килограмы в граммы тонны и центнер
	double m, it;
	cout << "Enter weight in kg: ";
	cin >> m;
	cout << "Enter the unit of mass 1-gram, 2-ton, 3-centner: ";
	cin >> mas;
	switch (mas)
	{

	case 1:
		it = m * 1000;
		cout << "Gram: " << it;
		break;
	case 2:
		it = m / 1000;
		cout << "Ton: " << it;
		break;
	case 3:
		it = m / 100;
		cout << "Centner: " << it;
		break;
	}


	_getch();
	fs.close();
	return 0;
}